var bounds;
var infowindow;
var marker, i;

function initMap() {
  bounds = new google.maps.LatLngBounds();
  infowindow = new google.maps.InfoWindow();

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: new google.maps.LatLng(40.730610, -73.935242),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
  });

  //https://github.com/jawj/OverlappingMarkerSpiderfier <=== borrowed from
  var overlappingMarkerSpiderfier = new OverlappingMarkerSpiderfier(map, {
    markersWontMove: true,
    markersWontHide: true,
    basicFormatEvents: true
  });
  var coodinateArr = [];
  overlappingMarkerSpiderfier.addListener('click', function(marker, event) {
    coodinateArr.push(marker.position);
    infowindow.setContent(marker.desc);
    infowindow.open(map, marker);
    if(coodinateArr.length==2){
      distance = calcDistance(coodinateArr[0], coodinateArr[1]);
      document.getElementById("distance").innerHTML = "Distance is: " + distance + " MI";
      coodinateArr = [];
    }
  });

  overlappingMarkerSpiderfier.addListener('spiderfy', function(markers) {
    infowindow.close();
  })

  var markers = [];
  // Create markers.
  if(locations.length > 0){
    locations.forEach(function(location) {
      var pos = new google.maps.LatLng(location.lat, location.lng);
      var pinColorHTMLCode = location.color;
      bounds.extend(pos);

      //displayCustomMarkers(location);
      var pinColor = pinColorHTMLCode.substr(1); //remove the # from the beginning
      var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
      new google.maps.Size(21, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(9, 9));
      var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
      new google.maps.Size(40, 37),
      new google.maps.Point(0, 0),
      new google.maps.Point(9, 9));
      var labelTxt = location.chr;
      if(labelTxt == null){
        labelTxt = "";
      }
      var marker = new google.maps.Marker({
        position: pos,
        label: {text: location.chr, color: "white"},
        icon: pinImage,
        shadow: pinShadow,
        labelAnchor: new google.maps.Point(30, 0),
        labelClass: "labels",
        map: map
      });
      markers.push(marker);
      marker.desc = location.info;
      overlappingMarkerSpiderfier.addMarker(marker);  // <-- here
    });
    map.fitBounds(bounds);
    var minClusterZoom = 14;
    var markerCluster = new MarkerClusterer(map, markers,
      {imagePath: 'img/m'});
      markerCluster.setMaxZoom(minClusterZoom);
    }

  }

  function calcDistance(p1, p2) {
    return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
  }

  document.addEventListener('DOMContentLoaded', function() {
   initMap();
}, false);
